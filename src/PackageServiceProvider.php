<?php

namespace Ata\Cycle\Permissions;

use Illuminate\Contracts\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Support\ServiceProvider;

class PackageServiceProvider extends ServiceProvider
{
    public function register()
    {
        app(Gate::class)->before(function (Authorizable $user, string $ability) {
            if (method_exists($user, 'hasPermissionTo')) {
                return $user->hasPermissionTo($ability) ?: null;
            }
        });
    }

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/cycle-permissions.php' => config_path('cycle-permissions.php')
        ]);

        $this->mergeConfigFrom(
            __DIR__ . '/../config/cycle-permissions.php', 'cycle-permissions'
        );
    }
}
