<?php


namespace Ata\Cycle\Permissions\Models;

use Ata\Cycle\ORM\Constrains\EmptyConstrain;
use Ata\Cycle\ORM\Models\CycleModel;
use Ata\Cycle\ORM\Models\Traits\IntPrimary;
use Ata\Cycle\ORM\Repositories\AtaRepository;
use Cycle\Annotated\Annotation\Entity;

/**
 * @Entity(constrain=EmptyConstrain::class, repository=AtaRepository::class)
 *
*/
class RolePermissionPivot extends CycleModel
{
    use IntPrimary;
}
