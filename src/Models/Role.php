<?php

namespace Ata\Cycle\Permissions\Models;

use Ata\Cycle\ORM\Models\CycleModel;
use Ata\Cycle\ORM\Models\Traits\IntPrimary;
use Ata\Cycle\ORM\Models\Traits\SoftDeletes;
use Ata\Cycle\ORM\Models\Traits\Timestamps;
use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Table;
use Cycle\Annotated\Annotation\Table\Index;
use Cycle\Annotated\Annotation\Relation\ManyToMany;

/**
 * @Entity
 *
 * @Table(
 *    indexes = {
 *       @Index(columns={"deleted_at", "id"}),
 *       @Index(columns={"deleted_at", "name"}),
 *    }
 * )
*/
class Role extends CycleModel
{
    use IntPrimary;
    use Timestamps;
    use SoftDeletes;

    /**
     * @Column(type="string")
    */
    public $name;

    /**
     * @ManyToMany(target=Permission::class, though=RolePermissionPivot::class)
    */
    public $permissions;

    public $users;
}
