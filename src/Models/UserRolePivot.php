<?php

namespace Ata\Cycle\Permissions\Models;

use Ata\Cycle\ORM\Models\CycleModel;
use Ata\Cycle\ORM\Models\Traits\IntPrimary;
use Ata\Cycle\ORM\Repositories\AtaRepository;
use Cycle\Annotated\Annotation\Entity;
use Ata\Cycle\ORM\Constrains\EmptyConstrain;

/**
 * @Entity(constrain=EmptyConstrain::class, repository=AtaRepository::class)
*/
class UserRolePivot extends CycleModel
{
    use IntPrimary;
}
