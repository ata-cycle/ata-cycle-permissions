<?php


namespace Ata\Cycle\Permissions\Generators;


use Ata\Cycle\Permissions\Models\UserRolePivot;
use Cycle\Schema\Definition\Relation;
use Cycle\Schema\GeneratorInterface;
use Cycle\Schema\Registry;

class AddUserRoleRelations implements GeneratorInterface
{
    /**
     * @inheritDoc
     */
    public function run(Registry $registry): Registry
    {
        $role = $registry->getEntity(config('cycle-permissions.role_class'));

        $relation = (new Relation())
            ->setTarget($registry->getEntity(config('cycle-permissions.user_class'))->getRole())
            ->setType('manyToMany')
            ->setInverse('roles', 'manyToMany');

        $relation->getOptions()
            ->set('though', $registry->getEntity(UserRolePivot::class)->getRole())
            ->set('cascade', null)
            ->set('nullable', null)
            ->set('innerKey', null)
            ->set('outerKey', null)
            ->set('where', null)
            ->set('thoughInnerKey', null)
            ->set('thoughOuterKey', null)
            ->set('thoughWhere', null)
            ->set('fkCreate', null)
            ->set('fkAction', null)
            ->set('indexCreate', null)
            ->set('load', null);

        $role->getRelations()->set('users', $relation);

        $role->getRelations()->get('permissions')->setInverse('roles', 'manyToMany');

        return $registry;
    }
}