<?php

namespace Ata\Cycle\Permissions\Traits;

use Ata\Cycle\Permissions\Models\Permission;
use Ata\Cycle\Permissions\Models\Role;

trait HasRoles
{
    public $roles;

    /**
     * Determine if the model may perform the given permission.
     *
     * @param $permission
     *
     * @return bool
     */
    public function hasPermissionTo($permission): bool
    {
        return resolve('cycle-db')->getRepository(Permission::class)
            ->where('roles.users.id', $this->id)
            ->whereIf(is_string($permission), 'name', $permission)
            ->whereIf(is_int($permission), 'id', $permission)
            ->whereIf($permission instanceof Permission, 'id', $permission)
            ->exists();
    }

    /**
     * @param $role
     * @return bool
     */
    public function hasRole($role): bool
    {
        return resolve('cycle-db')->getRepository(Role::class)
            ->where('users.id', $this->id)
            ->whereIf(is_string($role), 'name', $role)
            ->whereIf(is_int($role), 'id', $role)
            ->whereIf($role instanceof Role, 'id', $role)
            ->exists();
    }
}
