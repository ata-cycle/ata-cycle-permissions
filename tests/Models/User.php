<?php

namespace Ata\Cycle\Permissions\Tests\Models;

use Ata\Cycle\ORM\Models\CycleModel;
use Ata\Cycle\Permissions\Traits\HasRoles;
use Ata\Cycle\ORM\Models\Traits\IntPrimary;
use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;

/**
 * @Entity
*/
class User extends CycleModel
{
    use IntPrimary;
    use HasRoles;

    /**
     * @Column(type="string")
    */
    public $name;
}
