<?php

namespace Ata\Cycle\Permissions\Tests;

use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\Permissions\Generators\AddUserRoleRelations;
use Ata\Cycle\Permissions\Models\Role;
use Ata\Cycle\Permissions\Tests\Models\User;

abstract class TestCase extends BaseDBTestCase
{
    protected function getSourceClass()
    {
        return User::class;
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('cycle-permissions.user_class', User::class);
        $app['config']->set('cycle-permissions.role_class', Role::class);

        parent::getEnvironmentSetUp($app);

        $app['config']->set('cycle.schema.path', ['/app/tests/Models', '/app/src/Models']);

        $app['config']->set('cycle.schema.generators',
            array_merge(
                [new AddUserRoleRelations()],
                $app['config']->get('cycle.schema.generators'),
            )
        );

        $app['config']->set('cycle.commands.create', [
            new \Ata\Cycle\ORM\Mappers\Commands\Create\Create(),
            new \Ata\Cycle\ORM\Mappers\Commands\Create\Timestamps(),
        ]);
        $app['config']->set('cycle.commands.update', [
            new \Ata\Cycle\ORM\Mappers\Commands\Update\Update(),
            new \Ata\Cycle\ORM\Mappers\Commands\Update\Timestamps(),
        ]);
        $app['config']->set('cycle.commands.delete', [
            new \Ata\Cycle\ORM\Mappers\Commands\Delete\SoftDelete(),
        ]);
    }

    protected function getPackageProviders($app)
    {
        return array_merge(
            parent::getPackageProviders($app),
            [
                'Ata\Cycle\Permissions\PackageServiceProvider'
            ]
        );
    }
}
