<?php

namespace Ata\Cycle\Permissions\Tests\Unit;

use Ata\Cycle\Permissions\Models\Role;
use Ata\Cycle\Permissions\Tests\Models\User;
use Ata\Cycle\Permissions\Tests\TestCase;
use Cycle\ORM\Select\JoinableLoader;

class RoleTest extends TestCase
{
    protected function createEntities()
    {
        User::create([
            'name' => 'user1',
            'roles' => [
                ['name' => 'test_role_1'],
                [
                    'name' => 'test_role_2',
                    'guard_name' => 'api',
                    'permissions' => [
                        ['name' => 'test_perm1'],
                        ['name' => 'test_perm2']
                    ]
                ]
            ],
        ]);
        resolve('cycle-db')->getHeap()->clean();
    }

    // this test is somehow not working at the first time, but with existed entities it works
    public function testShouldCreateFullRoleMap()
    {
        $users = Role::orm()->select()->load('users', ['method' => JoinableLoader::LEFT_JOIN])->fetchAll()[0]->users;

        self::assertEquals(1, $users->count());
    }

    public function testShouldFindPermission()
    {
        $user = User::orm()->findOne();

        self::assertTrue($user->hasPermissionTo('test_perm1', 'api'));
    }

    public function testShouldNotFindPermission()
    {
        $user = User::orm()->findOne();

        self::assertFalse($user->hasPermissionTo('test_perm3', 'api'));
    }

    public function testShouldFindRole()
    {
        $user = User::orm()->findOne();

        self::assertTrue($user->hasRole('test_role_1', 'api'));
    }

    public function testShouldNotFindRole()
    {
        $user = User::orm()->findOne();

        self::assertFalse($user->hasRole('test_role_3', 'api'));
    }
}
